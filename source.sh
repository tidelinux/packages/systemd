# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/systemd/systemd/archive/v${BPM_PKG_VERSION}/systemd-${BPM_PKG_VERSION}.tar.gz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  sed -i -e 's/GROUP="render"/GROUP="video"/' \
         -e 's/GROUP="sgx", //' rules.d/50-udev-default.rules.in
  mkdir -p build
  cd build
  meson setup ..                 \
        --prefix=/usr            \
        --libdir=/usr/lib        \
        --buildtype=release      \
        -D default-dnssec=no     \
        -D firstboot=false       \
        -D install-tests=false   \
        -D ldconfig=false        \
        -D sysusers=true         \
        -D rpmmacrosdir=no       \
        -D homed=disabled        \
        -D userdb=false          \
        -D man=disabled          \
        -D mode=release          \
        -D pam=enabled           \
        -D pamconfdir=/etc/pam.d \
        -D dev-kvm-mode=0660     \
        -D nobody-group=nogroup  \
        -D sysupdate=disabled    \
        -D ukify=disabled        \
        -D docdir=/usr/share/doc/systemd
  ninja
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  cd build
  ninja test
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  cd build
  DESTDIR="$BPM_OUTPUT" ninja install
  systemd-machine-id-setup --root="$BPM_OUTPUT"
  systemctl --root="$BPM_OUTPUT" preset-all
}
